from flask import Flask, render_template, flash, redirect, url_for, request
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Email
from flask_bootstrap import Bootstrap
import db

app = Flask(__name__)
app.config.update(dict(
    SECRET_KEY="powerful secretkey",
    WTF_CSRF_SECRET_KEY="a csrf secret key"
))
Bootstrap(app)

class EmailForm(FlaskForm):
    name = StringField("Full name", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired(), Email()])

@app.route("/", methods=("GET", "POST"))
def index():
    form = EmailForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            db.save_subscriber(form.name.data, form.email.data)
            flash('Success!')
            return redirect(url_for('index'))
        else:
            flash("Invalid email")
            return redirect(url_for('index'))
    return render_template('index.html', form=form, request=request)



