import boto3 
import json

AWS_REGION = "ap-southeast-1"
TABLE_NAME = "Subscriber"

dynamodb = boto3.resource('dynamodb', region_name=AWS_REGION)
table = dynamodb.Table(TABLE_NAME)

def save_subscriber(name, email, table=table):
    response = table.put_item(
        Item={
            'Name': name,
            'Email': email
        }
    )
    print(json.dumps(response))

