# Startup Landing Page

A simple website that askes the user for name and email. Sometimes called as coming soon page. 

Click [here](https://landingpage.mirul.xyz/) to see it in action. 

Powered by [Zappa](https://github.com/Miserlou/Zappa), Flask, and Amazon Dynamodb. 

# Installation

Clone, virtualenv, requirements.txt. You know the drill.

Next, create a [DynamoDB table](https://console.aws.amazon.com/dynamodb/home) with 2 string attributes `Name` and `Email`

This is an example using aws cli
```
aws dynamodb create-table \
--table-name Subscriber \
--attribute-definitions \
	AttributeName=Name,AttributeType=S \
	AttributeName=Email,AttributeType=S \
--key-schema AttributeName=Email,KeyType=HASH AttributeName=Name,KeyType=RANGE \
--provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 \
```

Then, open up `db.py` and edit `AWS_REGION` and `TABLE_NAME`

Run locally with the normal flask command `FLASK_APP=main.py flask run`

Finally to deploy, `zappa init`, `zappa deploy`, and optionally `zappa certify`. 

And you're done! You now a have a completely server-less, no-ops, low-cost, infinately scalable landing page!

# Credits

Installation steps taken from [zappa-bittorrent-tracker](https://github.com/Miserlou/zappa-bittorrent-tracker)